import fetchAxios from '../utils/fetchAxios';

export default async function login(email, password) {
  try {
    const result = await fetchAxios('POST', 'user/login', {
      email,
      password
    });
    return result;
  } catch (error) {
    throw error;
  }
}