/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  ImageBackground,
  Image,
  Text,
  TextInput,
  View
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

import fetchAxios from '../../utils/fetchAxios';
import { PrimaryButton } from '../../components';

import styles from './LoginScreen-style';

import imgBackground from '../../assets/backgroundimg.jpeg';
const image = {uri: 'https://reactjs.org/logo-og.png'};


class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'irwin_prt@email.com',
      password: '12345678',
      movies: {},
    };
  }


  async fetchLogin(val) {
    const { navigation } = this.props;
    // try {
    //   let response = await fetch('https://api-nodejs-todolist.herokuapp.com/user/login', {
    //     method: 'POST',
    //     headers: {
    //       Accept: 'application/json',
    //       'Content-Type': 'application/json',
    //     },
    //     body: JSON.stringify(val),
    //   });
    //   let result = await response.json();

    //   if (response.status === 200) {
    //     AsyncStorage.setItem('@token', result.token);
    //     navigation.navigate('TabNavigator');
    //   } else {
    //     alert('error')
    //   }
    // } catch (error) {
    //   console.error(error);
    // }
    try {
      // const response = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/login', val);
      const response = await fetchAxios('POST', 'user/login', val);
      // console.log('res: ==222 ', response);

      AsyncStorage.setItem('@token', response.token);
      navigation.navigate('TabNavigator', {
        email: this.state.email,
      });

      // axios.post('https://api-nodejs-todolist.herokuapp.com/user/login', val)
      //   .then(response => {
      //     console.log(response);

      //     if (response.status !== 400) {
      //       AsyncStorage.setItem('@token', response.data.token);
      //       navigation.navigate('TabNavigator');
      //     } else {
      //       alert('error');
      //     }
      //   })
      //   .catch(err => console.error(err))
    } catch (e) {
      console.error(e);
    }
  }

  async handleLogin(val) {
    // const { email, password } = this.state;
    this.fetchLogin(val);
  }


  render() {
    // console.log('0');
    // console.log(this.state.movies);
    const { email, password } = this.state;
    return (
      <View style={styles.container}>
        <ImageBackground source={imgBackground} style={styles.imageBackground}>
          <View style={{alignItems: 'center', paddingTop: 40 }}>
            <Image source={image} style={styles.img} />

            <Text style={styles.title}>
              Saatnya bererkeasi yang bernilai edukasi {this.state.name}
            </Text>

            <View style={{ paddingTop: 20 }}>
              <TextInput
                style={{ height: 40, width: 300, color: '#FFF' }}
                underlineColorAndroid="#FFF"
                borderBottomWidth={0}
                placeholderTextColor="#FFF"
                placeholder="Email"
                onChangeText={(val) => this.setState({ email: val })}
                value={this.state.email}
              />
            </View>

            <View style={{ paddingTop: 20 }}>
              <TextInput
                style={{ height: 40, width: 300, color: '#FFF' }}
                underlineColorAndroid="#FFF"
                borderBottomWidth={0}
                placeholderTextColor="#FFF"
                placeholder="Kata Sandi"
                secureTextEntry
                onChangeText={(val) => this.setState({ password: val })}
                value={this.state.password}
              />
            </View>
          </View>

          <View style={{alignItems: 'center', paddingTop: 40 }}>
            <View style={{paddingBottom: 10, width: 300, borderRadius: 10}}>
              <PrimaryButton title="Masuk" onPress={() => this.handleLogin({email, password})} />
            </View>
            <View>
              <Text>Belum punya akun</Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

export default LoginScreen;
