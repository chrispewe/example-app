/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  Dimensions
} from 'react-native';

import { PrimaryButton } from '../../components';

import styles from './LandingScreen-style';

import imgBackground from '../../assets/backgroundimg.jpeg';
const image = {uri: 'https://reactjs.org/logo-og.png'};

const { height } = Dimensions.get('window');

function Landing(props) {
  const { navigation } = props;
  return (
    <View style={styles.container}>
      <ImageBackground source={imgBackground} style={styles.imageBackground}>
        <View style={{alignItems: 'center', paddingTop: height * 0.2}}>
          <Image source={image} style={styles.img} />

          <Text style={styles.title}>
            Aplikasi untuk rekreasi dan edukasi
          </Text>
          <Text style={styles.subtitle}>
            Redu App dapat menjadi sahabat terbaik kamu dalam mencari tempat
            wisata yang berfaedah dan penuh hikmah
          </Text>
        </View>

        <View style={styles.footer}>
          <View style={{paddingBottom: 10, width: 300, borderRadius: 10}}>
            <PrimaryButton title="Masuk" onPress={() => navigation.navigate('Login')} />
          </View>
          <View style={{paddingBottom: 10, width: 300}}>
            <PrimaryButton title="Daftar" onPress={() => console.log('Daftar')} />
          </View>
        </View>
      </ImageBackground>
    </View>
  )
}

export default Landing;
