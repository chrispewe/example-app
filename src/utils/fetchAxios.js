import  axios from 'axios';
import Config from 'react-native-config';


async function fetchAxios(method, path, body) {
  try {
    const req = await axios({
      method,
      url: `${Config.BASE_URL}/${path}`,
      // url: `https://api-nodejs-todolist.herokuapp.com/${path}`,
      data: body,
    });

    if (req.status !== 400) {
      return req;
    }
  } catch (e) {
    throw e;
  }

}

export default fetchAxios;
